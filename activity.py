from abc import ABC, abstractmethod

class Animal(ABC):
    def __init__(self, name, breed, age):
        self.name = name
        self.breed = breed
        self.age = age

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

    def call(self):
        pass

# DOG
class Dog(Animal):
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")


    def call(self):
        print(f"Here {self.name}!")

# CAT
class Cat(Animal):
    def eat(self, food):
        print(f"serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaaa!")

    def call(self):
        print(f"{self.name}, come on!")

# Test cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
